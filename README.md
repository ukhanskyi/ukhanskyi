# Hi, I'm Mykola 👋

I'm Mykola Ukhanskyi Software Developer with more than 3.5 years of experience in Ruby programming, using web framework Ruby on Rails. I am self-motivated. I come up with my own goals and execute them. I dive into new subjects and try to soak up knowledge in a short amount of time.

### Links

[![Portfolio Badge](https://img.shields.io/badge/Portfolio-af69ee?style=flat&logo=iconify&logoColor=white)](https://ukhanskyi-portfolio-mykola-ukhanskyi-0f5be573a156ee12fcdcb37014.gitlab.io/)
[![GitHub Badge](https://img.shields.io/badge/GitHub-black?style=flat&logo=github&logoColor=white)](https://github.com/Ukhanskyi)
[![LinkedIn Badge](https://img.shields.io/badge/LinkedIn-blue?style=flat&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/mykola-ukhanskyi-280a5b188/)
[![Instagram Badge](https://img.shields.io/badge/Instagram-E1306C?style=flat&logo=instagram&logoColor=white)](https://www.instagram.com/ukhanskyi/)
[![Gmail Badge](https://img.shields.io/badge/Email-red?style=flat&logo=gmail&logoColor=white)](mailto:ukhanskyi@gmail.com)

### Technologies

[![ruby Badge](https://img.shields.io/badge/Ruby-red?style=for-the-badge&logo=ruby&logoColor=red&labelColor=black&color=red)](https://www.ruby-lang.org/en/)
[![Rails Badge](https://img.shields.io/badge/Ruby%20on%20Rails-DC143C?style=for-the-badge&logo=rubyonrails&logoColor=DC143C&labelColor=black&color=DC143C)](https://rubyonrails.org/)
[![PostgreSQL Badge](https://img.shields.io/badge/PostgreSQL-0064a5?style=for-the-badge&logo=postgresql&logoColor=0064a5&labelColor=black&color=0064a5)](https://www.postgresql.org/)
[![MySQL Badge](https://img.shields.io/badge/mysql-00758f?style=for-the-badge&logo=mysql&logoColor=00758f&labelColor=black&color=00758f)](https://www.mysql.com/)
[![Swagger Badge](https://img.shields.io/badge/Swagger-85EA2D?style=for-the-badge&logo=swagger&logoColor=85EA2D&labelColor=black&color=85EA2D)](https://swagger.io/)
[![React Badge](https://img.shields.io/badge/React-61DAFB?style=for-the-badge&logo=react&logoColor=61DAFB&labelColor=black&color=61DAFB)](https://react.dev/)

### Support

I would appreciate your support here: [By me a coffe](https://www.buymeacoffee.com/ukhanskyic)

[<img src="https://gitlab.com/ukhanskyi/ukhanskyi/-/raw/main/bmc_qr.png?ref_type=heads" width="100"/>](https://gitlab.com/ukhanskyi/ukhanskyi/-/raw/main/bmc_qr.png?ref_type=heads)
